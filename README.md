# Js-wk3-iproject-qaboard

This is a question and answer board. Users and ask and answer questions via the web UI.

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM)
* [Bower](http://bower.io/)
* [Ember CLI](http://ember-cli.com/)

## Installation

* `git clone https://github.com/johnmbklein/js-wk3-iproject-qaboard.git`
* change into the new directory
* `npm install`
* `bower install`

## Running / Development

* `ember server`
* Visit your app at [http://localhost:4200](http://localhost:4200).

## Further Reading / Useful Links

* [ember.js](http://emberjs.com/)
* [ember-cli](http://ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)

## Support and contact details

Email with any questions or comments.

## Technologies Used

_Bootstrap_
_HTML_
_JavaScript_
_Ember_
_Node.js_
_Gulp_
_Bower_
_SASS/CSS_


### License

*This software is licensed under the GPL.  See license file for more information.*

Copyright (c) 2016 **_John Klein_**
