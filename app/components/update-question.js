import Ember from 'ember';

export default Ember.Component.extend({
  updateQuestion: false,
  actions: {
    updateQuestionFormShow() {
      this.set('updateQuestion', true);
    },
    updateQuestion(question) {
      var params = {
        author: this.get('author'),
        body: this.get('body'),
        notes: this.get('notes'),
      };
      this.set('updateQuestion', false)
      this.sendAction('updateQuestion', question, params)
    }
  }
});
